\include "articulate.ly"
\include "../include/global.ly"
\include "../include/harmony.ly"
\include "../include/outline.ly"
\include "../include/soprano.ly"

\score {
	\unfoldRepeats \articulate
	\new StaffGroup <<
		\new ChordNames {
			\harmony
		}
		\new Staff <<
			\set Staff.midiInstrument = "trumpet"
			\global \outline \soprano
		>>
	>>
	\midi {}
}
