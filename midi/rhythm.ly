\include "articulate.ly"
\include "../include/global.ly"
\include "../include/harmony.ly"
\include "../include/outline.ly"
\include "../include/rhythm.ly"

\score {
	\unfoldRepeats \articulate
	\new StaffGroup <<
		\new ChordNames {
			\harmonyR
		}
		\new Staff \relative c'' <<
			\global \outline \rhythmPO
		>>
	>>
	\midi {}
}
