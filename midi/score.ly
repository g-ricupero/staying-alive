\include "articulate.ly"
\include "../include/global.ly"
\include "../include/outline.ly"
\include "../include/harmony.ly"
\include "../include/soprano.ly"
\include "../include/alto.ly"
\include "../include/tenorA.ly"
\include "../include/tenorB.ly"
\include "../include/bass.ly"
\include "../include/ebass.ly"

\score {
	\unfoldRepeats \articulate
	\new StaffGroup <<
		% \new ChordNames {
		% 	\harmony
		% }
		\new Staff <<
			\set Staff.midiInstrument = "trumpet"
			\global \outline \soprano
		>>
		\new Staff <<
			\set Staff.midiInstrument = "alto sax"
			\global \outline \alto
		>>
		\new Staff <<
			\set Staff.midiInstrument = "tenor sax"
			\global \outline \tenorA
		>>
		\new Staff <<
			\set Staff.midiInstrument = "trombone"
			\global \outline \tenorB
		>>
		\new Staff <<
			\set Staff.midiInstrument = "baritone sax"
			\global \outline \bass
		>>
		% \new Staff \relative c, <<
		% 	\set Staff.midiInstrument = "electric bass (finger)"
		% 	\global \outline \ebass
		% >>
	>>
	\midi {}
}
