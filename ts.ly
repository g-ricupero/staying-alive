\include "include/global.ly"
\include "include/harmony.ly"
\include "include/outline.ly"
\include "include/tenorA.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Tenor Sax Bb"
	}
}

\score {
	\transpose c d <<
		% \new ChordNames {
		% 	\set chordChanges = ##t
		% 	\harmony
		% }
		\new Staff <<
			\clef treble
			\NoChords \global \outline \tenorA
		>>
	>>
	\layout {}
}
