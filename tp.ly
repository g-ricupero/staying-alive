\include "include/global.ly"
\include "include/harmony.ly"
\include "include/outline.ly"
\include "include/soprano.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Trumpet Bb"
	}
}

\score {
	\transpose c d <<
		% \new ChordNames {
		% 	\set chordChanges = ##t
		% 	\harmony
		% }
		\new Staff <<
			\clef treble
			\NoChords \global \outline \soprano
		>>
	>>
	\layout {}
}
